package me.duzhi.blog;

import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import me.duzhi.blog.hanlder.HoldHandler;
import me.duzhi.blog.hanlder.HtmlHandler;


/**
 * Copyright (c) 二月,28,2017 (ashang.peng@aliyun.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

public class Config extends io.jpress.Config {
    @Override
    public void onJPressStarted() {
        super.onJPressStarted();
    }

    public void configHandler(Handlers handlers) {
        handlers.add(new HoldHandler());
        handlers.add(new HtmlHandler());
        super.configHandler(handlers);
    }

    @Override
    public void configInterceptor(Interceptors interceptors) {
        interceptors.add(new me.duzhi.blog.interceptor.MemberInterceptor());
        super.configInterceptor(interceptors);
    }
}
