package me.duzhi.blog.listener;

import io.jpress.message.Actions;
import io.jpress.message.Message;
import io.jpress.message.MessageListener;
import io.jpress.message.annotation.Listener;
import io.jpress.model.User;
import me.duzhi.blog.model.DzLog;

/**
 * Copyright (c) 三月,03,2017 彭秦进 (ashang.peng@aliyun.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
@Listener(action = { Actions.USER_LOGINED })
public class LoginListener implements MessageListener {

    @Override
    public void onMessage(Message message) {
        if (message.getAction().equals(Actions.USER_LOGINED)) {
            User user = message.getData();
            DzLog dzLog = new DzLog();
            dzLog.setAction("LOGIN");
        }
    }
}
